# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields, Exclude, MatchMixin,
    sequence_ordered)
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from sql.conditionals import Coalesce
from sql.operators import Equal

__all__ = ['LocationCrossReference', 'Location']


class LocationCrossReference(sequence_ordered(), ModelSQL, ModelView,
        MatchMixin):
    """Location Cross Reference"""
    __name__ = 'stock.location.cross_reference'

    party = fields.Many2One('party.party', 'Party',
        select=True, ondelete='RESTRICT', depends=['party'])
    address = fields.Many2One('party.address', 'Address',
        domain=[('party', '=', Eval('party'))],
        ondelete='RESTRICT', depends=['party'])
    location = fields.Many2One('stock.location', 'Location',
        required=True, select=True, ondelete='RESTRICT')
    code = fields.Char('Code')
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('party_location_address_exclude', Exclude(t,
                (Coalesce(t.party, -1), Equal),
                (Coalesce(t.address, -1), Equal),
                (t.location, Equal)),
             'Combination of party, location and address must be unique.')]

    @fields.depends('location')
    def on_change_location(self):
        if self.location:
            self.name = self.location.name

    @classmethod
    def _get_reference(cls, pattern):
        Location = Pool().get('stock.location')

        domain = ['OR', ]
        context = pattern.copy() or {}
        match_order = Location.match_cross_reference_order()
        for fieldname in match_order:
            if fieldname:
                context[fieldname] = None
            subdomain = [(key, '=', value) for key, value in context.items()]
            domain.append(subdomain)
        ordering = [(fieldname, 'ASC NULLS LAST')
            for fieldname in match_order[::-1] if fieldname]

        records = cls.search(domain, order=ordering, limit=1)
        if records:
            record, = records
            context = pattern.copy()
            # check it matches
            for to_remove in match_order:
                if to_remove:
                    context[to_remove] = None
                if record.match(context, match_none=True):
                    return record


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    cross_references = fields.One2Many('stock.location.cross_reference',
        'location', 'Cross References')

    @classmethod
    def match_cross_reference_order(cls):
        return [None, 'address', 'party']

    def get_cross_reference(self, pattern):
        context = pattern.copy() or {}
        _pattern = {key_order: context.get(key_order, None)
            for key_order in self.match_cross_reference_order() if key_order}
        for to_remove in self.match_cross_reference_order():
            if to_remove:
                _pattern[to_remove] = None
            reference = self.reference_matches(_pattern)
            if reference:
                return reference

    def reference_matches(self, pattern):
        for reference in self.cross_references:
            if reference.match(pattern, match_none=True):
                return reference
        return False
