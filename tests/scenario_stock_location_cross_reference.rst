=======================================
Stock Location Cross Reference Scenario
=======================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install stock_location_cross_reference::

    >>> config = activate_modules('stock_location_cross_reference')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party 1')
    >>> party.save()
    >>> address = party.addresses[0]
    >>> address.name = 'Address 1'
    >>> address.save()


Get stock location::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])


Create Location Cross Reference::

    >>> CrossReference = Model.get('stock.location.cross_reference')
    >>> cross_reference = CrossReference()
    >>> cross_reference.party = party
    >>> cross_reference.address = party.addresses[0]
    >>> cross_reference.location = warehouse_loc
    >>> cross_reference.name == warehouse_loc.name
    True
    >>> cross_reference.save()


Duplicate Cross Reference::

    >>> cross_reference2 = CrossReference()
    >>> cross_reference2.party = party
    >>> cross_reference2.address = party.addresses[0]
    >>> cross_reference2.location = warehouse_loc
    >>> cross_reference2.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Combination of party, location and address must be unique. - 